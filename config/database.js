//Set up mongoose connection
const mongoose = require('mongoose');
const mongoDB = 'mongodb+srv://mongofishnet:fishnet12@fishnet-pjx5b.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(mongoDB,{ useNewUrlParser: true,useUnifiedTopology: true });
mongoose.Promise = global.Promise;

module.exports = mongoose;