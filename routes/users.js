const express = require('express');
const router = express.Router();
const userController = require('../app/api/controller/users');
const bodyParser = require('body-parser');

router.post('/register', userController.create);
router.post('/authenticate', userController.authenticate);
router.post('/updateprofile', userController.update_profile);

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

module.exports = router;