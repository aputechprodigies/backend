const express = require('express');
const router = express.Router();
const ewalletController = require('../app/api/controller/ewallet');
const bodyParser = require('body-parser');

router.get('/qr', ewalletController.ewallet.get_qr);
router.post('/scan', ewalletController.ewallet.scan_qr);
router.post('/topup', ewalletController.ewallet.topup);
router.post('/balance', ewalletController.ewallet.get_balance);
router.post('/purchase', ewalletController.ewallet.purchase_fish);

router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());

module.exports = router;