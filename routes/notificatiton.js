const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const notification = require('../app/api/controller/notification.js');

router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());

router.post('/send', notification.send_notification);
router.post('/', notification.get_notification);
router.get('/all', notification.get_all_notification);

module.exports = router;