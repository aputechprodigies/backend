const express = require('express');
const router = express.Router();
const fishPredict = require('../app/api/controller/fishnetML');
const bodyParser = require('body-parser');

router.post('/weather', fishPredict.getWeather);
router.get('/all/:index',fishPredict.allFish);
router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());


module.exports = router;