const express = require('express');
const router = express.Router();
const fishController = require('../app/api/controller/fishProduct');
const bodyParser = require('body-parser');
const multer =require('multer');

router.get('/fish/', fishController.getAll);
router.get('/fish/manage/', fishController.manage_fish);
router.post('/fish/', fishController.create);

router.get('/fish/:fishID', fishController.getById);
router.delete('/fish/:fishID', fishController.deleteById);

router.put('/fish/:fishID',multer({ dest: 'temp/', limits: { fieldSize: 8 * 1024 * 1024 } }).single(
    'image_file' ), fishController.updateById);


router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

module.exports = router;