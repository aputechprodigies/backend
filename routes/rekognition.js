const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const rekog = require('../app/api/controller/rekog.js');

router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());

rekog.set_purpose("rekognition");

router.post('/', rekog.rekognition.rekognise);
module.exports = router;