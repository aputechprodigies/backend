const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const chatbotController = require('../app/api/controller/chatbot.js');

router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());

router.post('/', chatbotController.inputText);
module.exports = router;