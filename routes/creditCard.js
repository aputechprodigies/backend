const express = require('express');
const router = express.Router();
const creditCardController = require('../app/api/controller/creditCard');
const bodyParser = require('body-parser');

router.get('/creditcard/:user_id', creditCardController.getAll);
router.post('/creditcard', creditCardController.create);
router.delete('/creditcard/:user_id',creditCardController.deleteById);

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

module.exports = router;