const express = require('express');
const logger = require('morgan');
const moment = require('moment');
const bodyParser = require('body-parser');
const mongoose = require('./config/database.js'); //database configuration
const serverTime = require('./app/api/helper/serverTime.js');
const cors = require('cors');
const users = require('./routes/users');
const ewallet = require('./routes/ewallet');
const chatbot = require('./routes/chatbot');
const rekognition = require('./routes/rekognition');
const io_config = require('./app/api/controller/ewallet');
const fishProduct = require('./routes/fishProduct')
const creditCard = require('./routes/creditCard');
const notificatiton = require('./routes/notificatiton')
const appsocket = express();
const http = require('http');
const server = http.createServer(appsocket);
const io = require('socket.io').listen(server);

io_config.qr(io);
io_config.setio(io);

const MLRouter = require('./routes/fishnet_ML');

var jwt = require('jsonwebtoken');
const cron = require("node-cron");
const app = express();

app.set('secretKey', 'fishnetatoscompetition'); // jwt secret token

// connection to mongodb
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(logger('dev'));
app.use(cors())
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// public route
app.use('/recognition', rekognition);
app.use('/users', users);
app.use('/chatbot', chatbot);
app.use('/product', fishProduct);
app.use('/payment', creditCard);


app.use('/notification', notificatiton);

//private route
app.use('/fish', validateUser, MLRouter);
app.use('/wallet', validateUser, ewallet);

app.get('/', function (req, res) {
    res.json({
        status: true,
        message: "Welcome to Fishnet"
    });
});

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            res.json({
                status: false,
                message: 'invalid token'
            });
        } else {
            // add user id to request
            req.body.userId = decoded.id;
            next();
        }
    });
}

app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// handle errors
app.use(function (err, req, res, next) {
    console.log(err);

    if (err.status === 404)
        res.status(404).json({
            status: false,
            message: "Not found"
        });
    else
        res.status(500).json({
            status: false,
            message: "Something looks wrong :( !!!"
        });
});

app.use("error", function (err) { // handle "error" event so nodejs will not crash
    console.log(err);
});


app.set('port', (process.env.PORT || 5000));

app.listen(app.get('port'), function () {
    console.log('Node server listening on port ' + app.get('port'));
    console.log(serverTime.getCurrentTime())
});

server.listen(3000, () => {

    console.log('Socket running on port 3000')

})