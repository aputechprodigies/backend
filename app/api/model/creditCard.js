const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;
const CreditCardSchema = new Schema({
    user_id: {
        type: String,
        trim: true,
        required: true,
    },
    brand: {
        type: String,
        trim: true,
        required: true,
    },
    country: {
        type: String,
        trim: false,
        required: true
    },
    exp_month: {
        type: Number,
        trim: true,
        required: true
    },
    exp_year: {
        type: Number,
        trim: true,
        required: true
    },
    fingerprint: {
        type: String,
        trim: true,
        required: true,
    },
    last4: {
        type: String,
        trim: true,
        required: true,
    },
    token: {
        type: String,
        trim: true,
        required: true,
    },
    created_at: {
        type: String,
        trim: true,
        required: true,
    }

});
module.exports = mongoose.model('credit_card', CreditCardSchema);