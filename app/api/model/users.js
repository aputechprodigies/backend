const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    email: {
        type: String,
        trim: true,
        required: true
    },
    password: {
        type: String,
        trim: true,
        required: true
    },
    mobile: {
        type: String,
        trim: true,
        required: false
    },
    profile_image: {
        type: String,
        trim: true,
        required: false
    },
    date_of_birth: {
        type: String,
        trim: true,
        required: false
    },
    address_line_1: {
        type: String,
        trim: true,
        required: false
    },
    address_line_2: {
        type: String,
        trim: true,
        required: false
    },
    city: {
        type: String,
        trim: true,
        required: false
    },
    postal_code: {
        type: String,
        trim: true,
        required: false
    },
    state: {
        type: String,
        trim: true,
        required: false
    },
    nric_number: {
        type: String,
        trim: true,
        required: false
    },
    about_me: {
        type: String,
        trim: true,
        required: false
    },
    device_identifier:{
        type: String,
        trim: true,
        required: false
    },
    device_information:{
        type: String,
        trim: true,
        required: false
    },
    device_type:{
        type: String,
        trim: true,
        required: false
    },
});

module.exports = mongoose.model('User', UserSchema);