const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    user_id: {
        type: String,
        trim: true,
        required: true,
    },
    balance: {
        type: String,
        trim: true,
        required: true
    },
    stripe_id: {
        type: String,
        trim: true,
        required: false
    },
    card: {
        type: String,
        trim: true,
        required: false
    }
});

module.exports = mongoose.model('ewallet', UserSchema);