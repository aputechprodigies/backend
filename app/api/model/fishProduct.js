const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const FishSchema = new Schema({
    title: {
        type: String,
        trim: true,
        required: true,
    },
    current_price: {
        type: Number,
        trim: true,
        required: true
    },
    weight: {
        type: String,
        trim: true,
        required: true,
    },
    created_at: {
        type: String,
        trim: true,
        required: true
    },
    seller_id:{
        type: String,
        trim: true,
        required: true
    },
    image_file: {
        type: String,
        trim: true,
        required: false
    },
});

module.exports = mongoose.model('product_fish', FishSchema);
