const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const fishSchema = new Schema({
  
    name: {
        type: String,
        trim: true,
        required: true
    },
    colour: {
        type: String,
        trim: true,
        required: false
    },
    location: {
        type: String,
        trim: true,
        required: false
    }
});

module.exports = mongoose.model('fish', fishSchema);