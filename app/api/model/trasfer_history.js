const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    date: {
        type: String,
        trim: true,
        required: true,
    },
    amount: {
        type: String,
        trim: true,
        required: true
    },
    reciepient_id: {
        type: String,
        trim: true,
        required: true
    },
    sender_id: {
        type: String,
        trim: true,
        required: true
    }
});

module.exports = mongoose.model('trasfer_history', UserSchema);