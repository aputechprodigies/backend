var sendNotification = function (data, callback) {

    var message = {
        app_id: "b74432b1-0437-4b22-8160-c7ab8becc28e",
        contents: data,
        included_segments: ["All"]
    };

    var headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic YjUzN2UxZWEtNDg4NS00MTRhLTg4MzQtNWNhOTBlMjc2Mzkz"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };

    var https = require('https');
    var req = https.request(options, function (res) {
        res.on('data', function (data) {
            console.log("Response:");
            console.log(JSON.parse(data));
            return callback(null);
        });
    });

    req.on('error', function (e) {
        console.log("ERROR:");
        console.log(e);
        return callback(e);
    });

    req.write(JSON.stringify(message));
    req.end();
};

module.exports = sendNotification