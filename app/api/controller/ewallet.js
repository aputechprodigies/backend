const userModel = require('../model/users');
const ewalletModel = require('../model/ewallet');
const trasfer_history_model = require('../model/trasfer_history');
const jwt = require('jsonwebtoken');
const stripe = require("stripe")("sk_test_fWpArvIegkyJ8AQ11qKesXaK003a8eSZ57");

var io = null;

exports.setio = (iof) => {
    io = iof;
}

var receiver;
var money;
var sender;
var sender_balance;
var receiver_balance;
var calculated;
var calculated1;
var flag = 10;

module.exports.ewallet = {

    //get the qr code
    get_qr: function (req, res, next) {

        var id = req.query.id;

        jwt.sign(id, 'livefornthgordieforsomething241623231624', (err, token) => {
            if (err) {
                console.log(err);
                next(err);
            } else {

                res.json({
                    qr_id: token,
                    status: "Successfully Authenticated"
                });
            }
        });
    },

    scan_qr: function (req, res, next) {

        var post_data = req.body;

        money = post_data.money;
        sender = post_data.sender;
        var qr = post_data.qr;

        jwt.verify(qr, 'livefornthgordieforsomething241623231624', function (err, decoded) {

            if (err) {
                res.json({
                    status: 'Invalid QR'
                });
            } else {

                receiver = decoded;

                ewalletModel.findOne({
                    user_id: receiver
                }, function (err, ewalletInfo) {
                    if (err) {
                        next(err);
                    } else {
                        if (ewalletInfo) {
                            receiver_balance = ewalletInfo.balance;
                        }
                    }
                });


                ewalletModel.findOne({
                    user_id: sender
                }, function (err, ewalletInfo) {
                    if (err) {
                        next(err);
                    } else {
                        if (ewalletInfo) {
                            sender_balance = ewalletInfo.balance;
                        }
                    }
                });

                userModel.findOne({
                    _id: sender
                }, function (err, userInfo) {
                    if (err) {
                        next(err);
                    } else {
                        if (userInfo) {

                            io.emit('approval' + receiver, userInfo.name, money, sender);

                            res.json({
                                status: "Waiting for reply"
                            });

                        }
                    }
                });
            }
        });
    },

    topup: function (req, res, next) {
        var post_data = req.body;
        var id = post_data.id;
        var balance;
        var customer;
        var money = post_data.money;
        var stripemoney = (parseInt(money)) * 100;

        ewalletModel.findOne({
            user_id: id
        }, function (err, ewalletInfo) {
            if (err) {
                next(err);
            } else {
                if (ewalletInfo) {
                    customer = ewalletInfo.stripe_id;
                    balance = ewalletInfo.balance;
                }
            }
        });


        const charge = stripe.charges.create({
            amount: stripemoney,
            currency: 'myr',
            customer: customer
        }, function (err, charge) {

            ewalletModel.updateOne({
                    user_id: id
                }, {
                    $set: {
                        balance: (parseFloat(balance) + parseFloat(money))
                    }
                },
                function (err, userInfo) {
                    if (err) {
                        next(err);
                    } else {
                        res.json({
                            status: "Succesfully updated",
                            balance: (parseFloat(balance) + parseFloat(money)).toString()
                        });
                    }
                });
        });
    },

    check_card: function (req, res, next) {
        var id = req.query.id;
        ewalletModel.findOne({
            user_id: id
        }, function (err, result) {
            if (err) {
                next(err);
            } else {
                if (result) {

                    if (result.stripe_id != "") {
                        res.json({
                            status: "exist",
                            card: result.card
                        });
                    } else {
                        res.json({
                            status: "not exist"
                        });
                    }
                }
            }
        });

    },

    get_balance: function (req, res, next) {
        var id = req.body.id;
        ewalletModel.findOne({
            user_id: id
        }, function (err, result) {
            if (err) {
                next(err);
            } else {
                if (result) {

                    res.json({
                        status: true,
                        balance: result.balance
                    })
                }
            }
        });

    },



    purchase_fish: function (req, res, next) {

        var buyer = req.body.id;
        var price = req.body.price;
        var seller = req.body.seller;
        var product_id = req.body.product_id;

        var final_balance;

        ewalletModel.findOne({
            user_id: buyer
        }, function (err, ewalletInfo) {
            if (err) {
                next(err);
            } else {
                if (ewalletInfo) {
                    if ((parseFloat(ewalletInfo.balance)) <= price) {
                        res.json({
                            status: false,
                            message: "Insufficient money. Please top-up.",
                            balance: ewalletInfo.balance
                        })
                    } else {
                        //seller adding on
                        ewalletModel.findOne({
                            user_id: seller
                        }, function (err, ewalletInfo) {
                            if (err) {
                                next(err);
                            } else {
                                if (ewalletInfo) {
                                    final_balance = (parseFloat(ewalletInfo.balance)) + (parseFloat(price));

                                    ewalletModel.updateOne({
                                            user_id: seller
                                        }, {
                                            $set: {
                                                balance: final_balance.toString()
                                            }
                                        },
                                        function (err, userInfo) {
                                            if (err) {
                                                next(err);
                                            }
                                        });
                                }
                            }
                        });


                        //buyer deduction
                        ewalletModel.findOne({
                            user_id: buyer
                        }, function (err, ewalletInfo) {
                            if (err) {
                                next(err);
                            } else {
                                if (ewalletInfo) {
                                    final_balance = (parseFloat(ewalletInfo.balance)) - (parseFloat(price));

                                    ewalletModel.updateOne({
                                            user_id: buyer
                                        }, {
                                            $set: {
                                                balance: final_balance.toString()
                                            }
                                        },
                                        function (err, userInfo) {
                                            if (err) {
                                                next(err);
                                            } else {

                                                res.json({
                                                    status: true,
                                                    message: "Purchase sucessful",
                                                    balance: final_balance.toString()
                                                })

                                            }
                                        });
                                }
                            }
                        });

                    }

                }
            }
        });



    }
};


exports.qr = (io) => {

    io.on('connection', function (socket) {

        console.log(socket.id + " is connected");
        socket.on('disc', function (id) {

            console.log('disconnected');
        });

        socket.on('status', function (status) {

            if (status.status == "accept") {

                /*for receiver*/

                ewalletModel.findOne({
                    user_id: status.receiver
                }, function (err, ewalletInfo) {
                    if (err) {
                        next(err);
                    } else {
                        if (ewalletInfo) {
                            receiver_balance = ewalletInfo.balance;
                        }
                    }
                });

                calculated = (parseFloat(receiver_balance)) + (parseFloat(status.money));

                ewalletModel.updateOne({
                        user_id: receiver
                    }, {
                        $set: {
                            balance: calculated.toString()
                        }
                    },
                    function (err, userInfo) {
                        if (err) {
                            next(err);
                        }
                    });


                /*for sender*/

                ewalletModel.findOne({
                    user_id: status.sender_id
                }, function (err, ewalletInfo) {
                    if (err) {
                        next(err);
                    } else {
                        if (ewalletInfo) {
                            sender_balance = ewalletInfo.balance;
                        }
                    }
                });


                calculated1 = (parseFloat(sender_balance)) - (parseFloat(status.money));

                ewalletModel.updateOne({
                        user_id: sender
                    }, {
                        $set: {
                            balance: calculated1.toString()
                        }
                    },
                    function (err, userInfo) {
                        if (err) {
                            next(err);
                        }
                    });

                check();

            } else {

                io.emit('status' + sender, 'Transaction has been rejected');
                console.log("rejected");

            }

        });
    });
}


function check() {
    if (flag >= 2) {

        var datetime = new Date();
        var date = (datetime.toISOString().slice(0, 10));

        trasfer_history_model.create({
            date: date,
            amount: money,
            reciepient_id: receiver,
            sender_id: sender
        }, function (err, result) {
            if (err) {
                next(err);
            } else {
                io.emit('status' + sender, "Successfully transfered", calculated1);
                io.emit('status' + receiver, "Successfully received", calculated);
            }
        });

    } else {
        io.emit('status' + sender, "Failed to transfer. Please try again");
        io.emit('status' + receiver, "Failed to transfer. Please try again");
        console.log("fail Update");
    }
}