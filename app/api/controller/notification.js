const OneSignal = require('../helper/OneSignal');
const OneSignalConfig = require('onesignal-node');
const serverTime = require('../helper/serverTime')
const form_data_s3 = require('../helper/form-data-s3');

const client = new OneSignalConfig.Client('b74432b1-0437-4b22-8160-c7ab8becc28e', 'YjUzN2UxZWEtNDg4NS00MTRhLTg4MzQtNWNhOTBlMjc2Mzkz');

module.exports = {

    send_notification: function (req, res, next) {

        form_data_s3.upload(req, res, "fishnettech", function (req, res) {
            if (req.file == undefined) {
                console.log('Error: No File Selected!');
                res.json('Error: No File Selected');
            } else {
                var type = req.body.type;
                var data = req.body.data;
                var subtitle = req.body.subtitle;
                var title = req.body.title;

                var message = {
                    "en": subtitle,
                    type: type,
                    title: title,
                    subtitle: subtitle,
                    data: data,
                    image_file: req.file.location,
                    time: serverTime.getCurrentTime()
                }

                OneSignal(message, function (err) {
                    if (err) {
                        console.log(err);
                        next();
                    } else {
                        res.json({
                            status: true,
                            message: 'successfully pushed'
                        });
                    }
                });
            }
        });

    },

    get_notification: async function (req, res, next) {

        var notification_id = req.body.notification_id;

        try {
            const response = await client.viewNotification(notification_id);
            res.json(response.body.contents);
        } catch (error) {
            console.log(error);
            next(error);
        }

    },

    get_all_notification: async function (req, res, next) {
        const response = await client.viewNotifications();

        let list = [];
        let sublist;
        var count = 0;

        response.body.notifications.forEach(element => {
            if (element.contents.type != null) {
                sublist = {
                    id: element.id,
                    type: element.contents.type,
                    title: element.contents.title,
                    subtitle: element.contents.subtitle,
                    time: element.contents.time
                };

                list.push(sublist);

                count = count + 1;
            }
        });

        res.json({
            notifications: list,
            total_count: count
        })
    }
};