const fishModel = require('../model/fishProduct');
const userModel = require('../model/users');
const serverTime = require('../helper/serverTime')

const aws = require('aws-sdk');
const fs = require('fs');

module.exports = {

    getById: function (req, res, next) {
        console.log(req.body);
        fishModel.findById(req.params.fishID, function (err, fishInfo) {
            if (err) {
                res.status(404).json({
                    status: false,
                    message: "No fish found",
                    product: fishInfo
                });

            } else {
                userModel.findById(fishInfo.seller_id, function (err, user) {
                    res.json({
                        status: true,
                        message: "Fish found",
                        product: fishInfo,
                        seller_info: {
                            seller_id: user._id,
                            name: user.name,
                            email: user.email,
                            profile_image: user.profile_image || null,
                            date_of_birth: user.date_of_birth || null,
                            address_line_1: user.address_line_1 || null,
                            address_line_2: user.address_line_2 || null,
                            city: user.city || null,
                            postal_code: user.postal_code || null,
                            state: user.state || null,
                            nric_number: user.nric_number || null,
                            about_me: user.about_me || null
                        }
                    })
                });
            }
        });
    },

    manage_fish: function (req, res, next) {
        fishModel.find({}, function (err, fish) {
            if (err) {
                next(err);
            } else {

                var fish_list = [];

                fish.forEach(element => {
                    if (element.seller_id == req.query.id) {
                        fish_list.push(element);
                    }
                });

                if (fish.length > 0) {
                    res.json({
                        status: true,
                        message: fish_list.length + " Fishes found",
                        product_list: fish_list
                    });
                } else {
                    res.json({
                        status: true,
                        message: "No Fish found !"
                    })
                }
            }
        });
    },

    getAll: function (req, res, next) {
        fishModel.find({}, function (err, fish) {
            if (err) {
                next(err);
            } else {

                var fish_list = [];

                fish.forEach(element => {
                    if (element.seller_id != req.query.id) {
                        fish_list.push(element);
                    }
                });

                if (fish.length > 0) {
                    res.json({
                        status: true,
                        message: fish_list.length + " Fishes found",
                        product_list: fish_list
                    });
                } else {
                    res.json({
                        status: true,
                        message: "No Fish found !"
                    })
                }
            }
        });
    },

    updateById: function (req, res, next) {

        aws.config.setPromisesDependency();
        aws.config.update({
            accessKeyId: "AKIAVTBIYANF3H6TDIDI",
            secretAccessKey: "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",
            region: "ap-southeast-1"
        });
        const s3 = new aws.S3();

        var params = {
            ACL: 'public-read',
            Bucket: "product-images-fishnet",
            Body: fs.createReadStream(req.file.path),
            Key: `uploadImage/${req.file.originalname}`
        };

        s3.upload(params, (err, data) => {
            if (err) {
                console.log('Error occured while trying to upload to S3 bucket', err);
            }

            if (data) {
                fs.unlinkSync(req.file.path); // Empty temp folder
                const locationUrl = data.Location;

                fishModel.updateOne({
                    _id: req.params.fishID
                }, {
                    $set: {
                        image_file: locationUrl
                    }
                }, function (err, fishInfo) {
                    if (err)
                        next(err);
                    else {
                        res.json({
                            status: true,
                            message: "Image Uploaded successfully",
                            product: fishInfo
                        });
                    }
                });

            }
        });

    },


    deleteById: function (req, res, next) {
        fishModel.findByIdAndRemove(req.params.fishID, function (err, fishInfo) {
            if (err)
                next(err);
            else {
                res.json({
                    status: true,
                    message: "Fish deleted successfully!!!",
                    product: null
                });
            }
        });
    },

    create: function (req, res, next) {
        fishModel.create({
            title: req.body.title,
            current_price: req.body.current_price,
            created_at: serverTime.getCurrentTime(),
            seller_id: req.body.seller_id,
            weight: req.body.weight,
            image_file: req.body.image_file
        }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({
                    status: true,
                    message: "Fish added successfully!!!",
                    product: result
                });
        });
    }
};