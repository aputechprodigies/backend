const AWS = require('aws-sdk');
const accessKey = 'AKIAVTBIYANF6NU2S4X5';
const secretKey = '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';
const region1 = 'us-east-1';
const fish = require('../model/fish.js')
const rekog = require('../controller/rekog');
const form_data_s3 = require('../helper/form-data-s3');

module.exports = {
    inputText: (function (req, res, next) {

        rekog.set_purpose("chatbot");

        var lexruntime = new AWS.LexRuntime({
            accessKeyId: accessKey,
            secretAccessKey: secretKey,
            region: region1
        });

        form_data_s3.upload(req, res, "fishnettech", function (req, res) {
            if (req.file != undefined) {
                rekog.rekognition.rekognise(req, res, next);
            } else if (req.body.input_text != '') {

                var text = req.body.input_text;

                var params = {
                    botAlias: 'FishNetAlias',
                    botName: 'FishNetChatBot',
                    inputText: text.toLowerCase(),
                    userId: 'aUniqueUserID' /* required */

                };

                lexruntime.postText(params, function (err, data) {
                    if (err)
                        console.log(err, err.stack);
                    // an error occurred
                    else
                        var sameIntentName = validate('EndangeredSpecies', data.intentName);
                    var sameDialogueState = validate('Fulfilled', data.dialogState);

                    if (sameIntentName == 0 && sameDialogueState == 0)

                        fish.findOne({
                            name: data.slots.fishName
                        }, function (err, fishInfo) {
                            if (err) {
                                next(err);
                            } else {
                                if (!fishInfo) {
                                    res.json({
                                        status: true,
                                        message: "Response has been retrieved",
                                        data: titleCase(data.slots.fishName) + " is not an Endangered Species"

                                    });
                                } else {
                                    res.json({
                                        status: true,
                                        message: "Response has been retrieved",
                                        data: titleCase(data.slots.fishName) + " is an Endangered Species",
                                        url: fishInfo.location
                                    });
                                }
                            }
                        });
                    else

                        res.json({
                            status: true,
                            message: "Response has been retrieved",
                            data: data.message
                        });
                });
            } else {
                res.json({
                    status: false,
                    message: "No message has been sent"
                });
            }
        });

        const validate = (firstValue, secondValue) => {

            var finalResults = firstValue.localeCompare(secondValue);
            return finalResults
        }

    })
}

function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
}