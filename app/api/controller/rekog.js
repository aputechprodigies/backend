const aws = require('aws-sdk');
const fishPredict = require('./fishnetML');
const form_data_s3 = require('../helper/form-data-s3');

var purpose = 'rekognition'

exports.set_purpose = (purpose_from_action) => {
    purpose = purpose_from_action;
}

module.exports.rekognition = {

    rekognise: function (req, res, next) {

        form_data_s3.upload(req, res, "fishnettech", function (req, res) {
            if (req.file == undefined) {
                console.log('Error: No File Selected!');
                res.json('Error: No File Selected');
            } else {

                const imageName = req.file.key;
                const imageLocation = req.file.location;

                var flag = 0;

                aws.config.loadFromPath('./config/config.json');

                var client = new aws.Rekognition({
                    apiVersion: '2016-06-27'
                });


                const params = {
                    Image: {
                        S3Object: {
                            Bucket: 'fishnettech',
                            Name: imageName
                        },
                    },
                    MaxLabels: 10
                }

                client.detectLabels(params, function (err, response) {
                    if (err) {
                        console.log(err, err.stack); // an error occurred
                    } else {

                        response.Labels.forEach(label => {

                            if ((label.Name == "Fish" && label.Confidence >= 90.0) || (label.Name == "Seafood" && label.Confidence >= 90.0)) {

                                flag = 1;
                            }
                        })

                        if (flag == 1) {

                            fishPredict.predictSpecies(imageLocation, imageName, purpose, res, function (err) {
                                console.log(err)
                            })

                        } else {

                            form_data_s3.deleteS3('fishnettech',imageName);

                            if (purpose == 'rekognition') {
                                res.json({
                                    status: false,
                                    message: "No fish detected"
                                });
                            } else {

                                purpose = 'rekognition';

                                res.json({
                                    status: true,
                                    message: "Response has been retrieved",
                                    data: "No fish detected"
                                });
                            }

                        }
                    }
                });
            }
        });

    }
}