const stripe = require("stripe")("sk_test_fWpArvIegkyJ8AQ11qKesXaK003a8eSZ57");
const serverTime = require('../helper/serverTime')
const userModel = require('../model/users');
const creditCardModel = require('../model/creditCard');
const ewalletModel = require('../model/ewallet');

module.exports = {

    getAll: function (req, res, next) {
        creditCardModel.findOne({
            user_id: req.params.user_id
        }, function (err, result) {
            if (err) {
                res.json({
                    status: false,
                    message: "No credit card found",

                });

            } else if (result == null) {
                res.json({
                    status: false,
                    message: "No credit card found",

                });

            } else {
                res.json({
                    status: true,
                    message: "Credit Card found",
                    card: result,
                });

            }
        })
    },

    deleteById: function (req, res, next) {

        ewalletModel.findOne({
            user_id: req.params.user_id
        }, function (err, result) {
            if (err) {
                next(err);
            } else {
                if (result) {

                    stripe.customers.del(
                        result.stripe_id,
                        function (err, confirmation) {

                            if (err) {
                                console.log(err);
                                next(err);
                            } else {

                                ewalletModel.updateOne({
                                        user_id: req.params.user_id
                                    }, {
                                        $set: {
                                            card: "",
                                            stripe_id: ""
                                        }
                                    },
                                    function (err, userInfo) {
                                        if (err) {
                                            next(err);
                                        }
                                    });
                            }
                        }
                    );
                }
            }
        });

        creditCardModel.findOneAndRemove({
            user_id: req.params.user_id
        }, function (err, creditCard) {
            if (err)
                next(err);
            else {

                res.json({
                    status: true,
                    message: "Credit Card deleted successfully!!!",

                });
            }
        });
    },

    create: function (req, res, next) {
        var email;
        creditCardModel.findOne({
                user_id: req.body.user_id
            }, function (err, userInfo) {
                if (err) {
                    next(err);

                } else {
                    if (userInfo) {
                        return res.status(400).send({
                            status: false,
                            message: 'Credit card already added.'
                        });
                    } else {

                        userModel.findOne({
                            _id: req.body.user_id
                        }, function (err, result) {
                            if (err) {
                                next(err);
                            } else {
                                if (result) {
                                    email = result.email;
                                } else {
                                    res.json({
                                        status: "Invalid user"
                                    });
                                }
                            }
                        });

                        stripe.customers.create({
                            source: req.body.stripetoken,
                            email: email

                        }, function (err, token) {
                            // asynchronously called
                            if (err) {
                                res.json({
                                    status: false,
                                    message: err.message
                                })
                            } else {

                                ewalletModel.updateOne({
                                        user_id: req.body.user_id
                                    }, {
                                        $set: {
                                            card:  token.sources.data[0].last4,
                                            stripe_id: token.id
                                        }
                                    },
                                    function (err, userInfo) {
                                        if (err) {
                                            next(err);
                                        }
                                    });

                                creditCardModel.create({
                                    user_id: req.body.user_id,
                                    brand: token.sources.data[0].brand,
                                    country: token.sources.data[0].country,
                                    exp_month: token.sources.data[0].exp_month,
                                    exp_year: token.sources.data[0].exp_year,
                                    fingerprint: token.sources.data[0].fingerprint,
                                    token: token.sources.data[0].id,
                                    last4: token.sources.data[0].last4,
                                    created_at: serverTime.getCurrentTime()
                                }, function (err, result) {
                                    if (err)
                                        next(err);
                                    else
                                        res.json({
                                            status: true,
                                            message: "Successfully added credit card",
                                            card: {
                                                user_id: result.user_id,
                                                brand: result.brand,
                                                country: result.country,
                                                exp_month: result.exp_month,
                                                exp_year: result.exp_year,
                                                fingerprint: result.fingerprint,
                                                token: result.token,
                                                last4: result.last4,
                                                created_at: result.created_at
                                            },
                                            ori: token
                                        })
                                });


                            }
                        });

                    }
                }
            }

        )
    }

};