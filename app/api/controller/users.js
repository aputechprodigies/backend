const userModel = require('../model/users');
const ewalletModel = require('../model/ewallet');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const form_data_s3 = require('../helper/form-data-s3');

module.exports = {
    create: function (req, res, next) {
        userModel.findOne({
            email: req.body.email
        }, function (err, userInfo) {
            if (err) {
                next(err);

            } else {
                if (userInfo) {
                    res.status({
                        status: false,
                        message: 'User already exists!'
                    });
                } else {
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(req.body.password, salt, (err, hash) => {
                            if (err) {
                                console.log(err);
                            } else {

                                userModel.create({
                                    name: req.body.name,
                                    email: req.body.email,
                                    password: hash,
                                    mobile: req.body.mobile,
                                    device_identifier: req.body.device_identifier,
                                    device_information: req.body.device_information,
                                    device_type: req.body.device_type
                                }, function (err, result) {
                                    if (err) {
                                        next(err);
                                    } else {

                                        ewalletModel.create({
                                            user_id: result._id,
                                            balance: '0',
                                            stripe_id: "",
                                            card: ""
                                        }, function (err, result) {
                                            if (err) {
                                                next(err);
                                            } else {

                                            }
                                        });

                                        res.json({
                                            status: true,
                                            message: "User added successfully!!!",
                                            user: result
                                        });
                                    }
                                });
                            }
                        })
                    })
                }
            }
        });
    },

    authenticate: function (req, res, next) {
        userModel.findOne({
            email: req.body.email
        }, function (err, userInfo) {
            if (err) {
                next(err);
            } else {
                if (userInfo) {
                    if (bcrypt.compareSync(req.body.password, userInfo.password)) {
                        const token = jwt.sign({
                            id: userInfo._id
                        }, req.app.get('secretKey'), {});

                        /* notifModel.findOne({
                            user_id:userInfo._id
                        },function(err,notif){

                            if(!notif){
                                notifModel.create({
                                    user_id:userInfo._id,
                                    player_id:req.body.player_id
                                })
                            }else{
                                notifModel.findOneAndUpdate({
                                    user_id:userInfo.id
                                },{player_id:req.body.player_id})
                            }
                        }) */

                        ewalletModel.findOne({
                            user_id: userInfo._id
                        }, function (err, result) {
                            if (err) {
                                next(err);
                            } else {
                                if (result) {
                                    res.json({
                                        status: true,
                                        message: "Successfully logged in. Welcome " + userInfo.name + ".",
                                        user: {
                                            _id: userInfo._id || null,
                                            name: userInfo.name || null,
                                            email: userInfo.email || null,
                                            password: userInfo.password || null,
                                            balance: result.balance,
                                            profile_image: userInfo.profile_image || null,
                                            date_of_birth: userInfo.date_of_birth || null,
                                            address_line_1: userInfo.address_line_1 || null,
                                            address_line_2: userInfo.address_line_2 || null,
                                            city: userInfo.city || null,
                                            postal_code: userInfo.postal_code || null,
                                            state: userInfo.state || null,
                                            nric_number: userInfo.nric_number || null,
                                            about_me: userInfo.about_me || null,
                                            device_info: {
                                                device_identifier: req.body.device_identifier || null,
                                                device_information: req.body.device_information || null,
                                                device_type: req.body.device_type || null
                                            }
                                        },
                                        function (err, result) {},
                                        token: token
                                    });
                                }
                            }
                        });


                    } else {
                        res.json({
                            status: false,
                            message: "Invalid email/password!!!"
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: "Invalid email/password!!!"
                    });
                }

            }
        });
    },

    update_profile: function (req, res, next) {

        form_data_s3.upload(req, res, "fishnet-profile", function (req, res) {
            if (req.file == undefined) {
                res.json({
                    status: false,
                    message: "No image",
                });
            } else {

                userModel.updateOne({
                    _id: req.body.id
                }, {
                    $set: {
                        profile_image: req.file.location,
                        date_of_birth: req.body.date_of_birth,
                        address_line_1: req.body.address_line_1,
                        address_line_2: req.body.address_line_2,
                        city: req.body.city,
                        postal_code: req.body.postal_code,
                        state: req.body.state,
                        nric_number: req.body.nric_number,
                        about_me: req.body.about_me
                    }
                }, function (err, result) {
                    if (err)
                        next(err);
                    else {
                        res.json({
                            status: true,
                            message: "Successfully updated",
                        });
                    }
                });

            }
        })

    }
};