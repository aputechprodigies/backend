const tf = require('@tensorflow/tfjs');
const automl = require('@tensorflow/tfjs-automl');
const jpeg = require('jpeg-js');
const request = require('request');
const fs = require('fs');
const fish = require('../model/fish.js');
const rekog = require('./rekog');
const OneSignal = require('../helper/OneSignal');
const serverTime = require('../helper/serverTime')
const weather=require('../../../config/weather')


module.exports = {

  predictSpecies: function (url, key, purpose, res, next) {

    predict();

    async function predict() {
      try {
        const model = await automl.loadImageClassification('https://fishnet-ml-model.s3-ap-southeast-1.amazonaws.com/model.json');


        var file = download(url, key, async function () {
          console.log(file);

          const image = readImage(file);

          const input = imageToInput(image, 3);

          const predictions = await model.classify(input);
          // console.log(predictions);
          let labelArray = [];
          let probabilityArray = [];
          for (let product of predictions) {
            labelArray.push(product.label);
            probabilityArray.push(product.prob);
          }

          var max = probabilityArray[0];
          var maxIndex = 0;

          for (var i = 1; i < probabilityArray.length; i++) {
            if (parseFloat(probabilityArray[i]) > max) {
              maxIndex = i;
              max = probabilityArray[i];
            }
          }

          var fishName = labelArray[maxIndex];
          var fishNameArr = fishName.toString().split('_');
          console.log(fishNameArr[fishNameArr.length - 1]);
          var finalFishName = '';

          for (var a = 0; a <= fishNameArr.length - 2; a++) {
            finalFishName = finalFishName + ' ' + fishNameArr[a];
          }
          finalFishName = finalFishName.substr(1);
          console.log(finalFishName);

          if (probabilityArray[maxIndex] > 0.5) {

            if (fishNameArr[fishNameArr.length - 1] == "Red") {

              var message = {
                "en": finalFishName + " is an endangered fish.",
                type: "Endangered",
                title: "Endangered Alert",
                subtitle: finalFishName + " is an endangered fish.",
                url: url,
                fish_name: finalFishName,
                index: fishNameArr[fishNameArr.length - 1],
                time: serverTime.getCurrentTime()
              }

              OneSignal(message, (err) => {
                if (err) {
                  console.log(err);
                }
              });

            }

            if (purpose == 'rekognition') {

              res.status(200).json({
                status: true,
                message: "Fish found in knowledge base",
                fish: finalFishName,
                location: url,
                probability: probabilityArray[maxIndex],
                index: fishNameArr[fishNameArr.length - 1],
              });
            } else {
              res.json({
                status: true,
                message: "Response has been retrieved",
                data: "This is a " + finalFishName + ". An endangered fish."
              });
            }

          } else {

            if (purpose == 'rekognition') {
              res.status(200).json({
                status: true,
                message: "Fish not found in knowledge base",
                location: url,
                index: "Green",
              });
            } else {

              rekog.set_purpose("rekognition");
              res.json({
                status: true,
                message: "Response has been retrieved",
                data: "This is not an endangered fish."
              });
            }

          }

          const path = file;

          try {
            fs.unlinkSync(path)
            //file removed
          } catch (err) {
            console.error(err)
          }

        });

      } catch (err) {
        console.log(err);
      }

    }

    var download = function (uri, filename, callback) {
      request.head(uri, function (err, res, body) {
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
      });
      return filename;
    };


    const readImage = path => {
      const buf = fs.readFileSync(path)
      const pixels = jpeg.decode(buf, true)
      return pixels
    }

    const imageToInput = (image, numChannels) => {
      const values = imageByteArray(image, numChannels)
      const outShape = [image.height, image.width, numChannels];
      const input = tf.tensor3d(values, outShape, 'int32');

      return input
    }

    const imageByteArray = (image, numChannels) => {
      const pixels = image.data
      const numPixels = image.width * image.height;
      const values = new Int32Array(numPixels * numChannels);

      for (let i = 0; i < numPixels; i++) {
        for (let channel = 0; channel < numChannels; ++channel) {
          values[i * numChannels + channel] = pixels[i * 4 + channel];
        }
      }

      return values
    }

  },

  allFish: function (req, res, next) {

    fish.find({
        colour: req.params.index
      })
      .then(fishdetails => {



        res.json({
          status: true,
          message: "All fish details are retrieved",
          info: fishdetails
        });
      }).catch(err => {
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving fish details."
        });
      });
  },

  getWeather: function (req, res, next) {

    weather.getCurrentWeatherByGeoCoordinates(req.body.lat, req.body.long, (err, currentWeather) => {
      if (err) {
        console.log(err);
      } else {
        res.json({
          status: "True",
          message: "Message Details have be retrieved",
          weatherDetails: {
            weather: currentWeather.weather[0].main,
            description: currentWeather.weather[0].description,
            country: currentWeather.sys.country,
            location: currentWeather.name
          }
        })
      }
    });
  }

}